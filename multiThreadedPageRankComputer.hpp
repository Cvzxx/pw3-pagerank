#ifndef SRC_MULTITHREADEDPAGERANKCOMPUTER_HPP_
#define SRC_MULTITHREADEDPAGERANKCOMPUTER_HPP_

#include <atomic>
#include <mutex>
#include <shared_mutex>
#include <thread>

#include "immutable/network.hpp"
#include "immutable/pageIdAndRank.hpp"
#include "immutable/pageRankComputer.hpp"
#include <condition_variable>
#include <future>
#include <queue>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using edges_map = std::unordered_map<PageId, std::vector<PageId>, PageIdHash>;
using hash_map = std::unordered_map<PageId, PageRank, PageIdHash>;
using num_links = std::unordered_map<PageId, uint32_t, PageIdHash>;
using map_iter = std::unordered_map<PageId, PageRank, PageIdHash>::iterator;
using dangling_set = std::unordered_set<PageId, PageIdHash>;
using set_iter = std::unordered_set<PageId, PageIdHash>::iterator;

class SimplePool {
private:
    uint32_t iter;
    mutable std::mutex mt;

    mutable map_iter mapIter;

public:
    SimplePool()
        : iter(0)
    {
    }

    void setIterI(uint32_t val)
    {
        iter = val;
    }

    void setIteratorM(hash_map& pageHashMap)
    {
        mapIter = pageHashMap.begin();
    }

    void
    fun1(Network const& network, hash_map& pageHashMap, num_links& numLinks,
        dangling_set& danglingNodes, double& dangleSum)
    {
        uint32_t myVal;
        while (true) {
            std::unique_lock<std::mutex> lck(mt);
            myVal = iter;
            iter++;
            lck.unlock();

            if (myVal >= network.getSize())
                return;

            network.getPages()[myVal].generateId(network.getGenerator());
            auto page = network.getPages()[myVal];

            lck.lock();

            pageHashMap[page.getId()] = 1.0 / network.getSize();

            numLinks[page.getId()] = page.getLinks().size();

            if (page.getLinks().size() == 0) {
                danglingNodes.insert(page.getId());
                dangleSum += (1.0 / network.getSize());
            }

            lck.unlock();
        }
    }

    void fun2(Network const& network,
        edges_map& edges, hash_map& previousPageHashMap,
        hash_map& pageHashMap, num_links& numLinks,
        double& difVal, double& dangVal,
        std::vector<PageIdAndRank>& result, dangling_set& danglingNodes, uint32_t start, uint32_t end,
        double dangleSum, double alpha)
    {

        double difference = 0;
        map_iter myIter = pageHashMap.begin();
        std::advance(myIter, start);

        uint32_t myId = start;

        if (start == end)
            return;

        while (true) {

            PageId pageId = myIter->first;
            PageRank* rank = &myIter->second;

            double danglingWeight = 1.0 / network.getSize();
            *rank = dangleSum * danglingWeight + (1.0 - alpha) / network.getSize();

            if (edges.count(pageId) > 0) {
                for (auto link : edges[pageId]) {
                    *rank += alpha * previousPageHashMap[link] / numLinks[link];
                }
            }
            difference += std::abs(
                previousPageHashMap[pageId] - pageHashMap[pageId]);

            result[myId] = PageIdAndRank(pageId, *rank);

            if (danglingNodes.count(pageId) > 0)
                dangVal += *rank;

            myId++;
            if (myId == end) {
                break;
            }
            myIter++;
        }

        difVal = difference;
    }
};

void startThread1(SimplePool& smp, Network const& network,
    edges_map& edges, hash_map& previousPageHashMap,
    hash_map& pageHashMap, num_links& numLinks,
    double& difVal, double& dangVal,
    std::vector<PageIdAndRank>& result,
    dangling_set& danglingNodes, uint32_t start, uint32_t end, double dangleSum,
    double alpha)
{
    smp.fun2(network, edges, previousPageHashMap, pageHashMap,
        numLinks, difVal, dangVal, result, danglingNodes, start, end, dangleSum,
        alpha);
}

void startThread(SimplePool& smp, Network const& network, hash_map& pageHashMap,
    num_links& numLinks, dangling_set& danglingNodes, double& dangleSum)
{
    smp.fun1(network, pageHashMap, numLinks, danglingNodes, dangleSum);
}

class MultiThreadedPageRankComputer : public PageRankComputer {
public:
    MultiThreadedPageRankComputer(uint32_t numThreadsArg)
        : numThreads(numThreadsArg){};

    std::vector<PageIdAndRank>
    computeForNetwork(Network const& network, double alpha,
        uint32_t iterations,
        double tolerance) const
    {
        //

        std::unordered_map<PageId, uint32_t, PageIdHash> numLinks;
        std::unordered_set<PageId, PageIdHash> danglingNodes;

        std::vector<std::thread> threads(numThreads);
        std::vector<uint32_t> counter;
        counter.assign(numThreads, 0);
        std::unordered_map<PageId, PageRank, PageIdHash> pageHashMap;

        std::unordered_map<PageId, std::vector<PageId>, PageIdHash> edges;
        std::vector<double> dangligValues(numThreads, 0.0);
        std::vector<double> difValues(numThreads, 0.0);
        SimplePool smp;

        double dangleSum = 0;
        double difference = 0;

        for (uint32_t i = 0; i < numThreads; ++i)
            threads[i] = std::thread{ startThread, std::ref(smp), std::ref(network), std::ref(pageHashMap),
                std::ref(numLinks), std::ref(danglingNodes), std::ref(dangligValues[i]) };

        for (uint32_t i = 0; i < numThreads; ++i)
            threads[i].join();

        for (uint32_t j = 0; j < numThreads; ++j) {
            dangleSum += dangligValues[j];
            dangligValues[j] = 0.0;
        }

        std::vector<std::pair<PageId, PageId>> vectorEdges;
        uint32_t idx = 0;
        for (auto page : network.getPages()) {
            counter[idx]++;
            idx = (idx + 1) % numThreads;
            for (auto link : page.getLinks()) {
                edges[link].push_back(page.getId());
                vectorEdges.push_back({ link, page.getId() });
            }
        }

        for (uint32_t j = 1; j < numThreads; ++j)
            counter[j] += counter[j - 1];

        for (uint32_t i = 0; i < iterations; ++i) {
            std::unordered_map<PageId, PageRank, PageIdHash> previousPageHashMap = pageHashMap;
            smp.setIterI(0);
            smp.setIteratorM(pageHashMap);

            std::string empty = "";
            std::vector<PageIdAndRank> result(pageHashMap.size(),
                PageIdAndRank(empty, 0));

            difference = 0.0;
            dangleSum = dangleSum * alpha;

            for (uint32_t j = 0; j < numThreads; ++j) {

                uint32_t start = 0;
                uint32_t end = 0;

                if (j > 0) {
                    start = counter[j - 1];
                }

                end = counter[j];
                threads[j] = std::thread{ startThread1, std::ref(smp),
                    std::ref(network), std::ref(edges),
                    std::ref(previousPageHashMap),
                    std::ref(pageHashMap),
                    std::ref(numLinks),
                    std::ref(difValues[j]),
                    std::ref(dangligValues[j]),
                    std::ref(result),
                    std::ref(danglingNodes), start, end,
                    dangleSum, alpha };
            }

            dangleSum = 0.0;
            for (uint32_t j = 0; j < numThreads; ++j)
                threads[j].join();

            for (uint32_t j = 0; j < numThreads; ++j) {
                difference += difValues[j];
                dangleSum += dangligValues[j];
                dangligValues[j] = 0.0;
                difValues[j] = 0.0;
            }

            ASSERT(result.size() == network.getSize(),
                "Invalid result size=" << result.size()
                                       << ", for network"
                                       << network);

            if (difference < tolerance) {
                return result;
            }
        }

        ASSERT(false,
            "Not able to find result in iterations=" << iterations);
    }

    std::string getName() const
    {
        return "MultiThreadedPageRankComputer[" + std::to_string(this->numThreads) + "]";
    }

private:
    uint32_t numThreads;
};

#endif /* SRC_MULTITHREADEDPAGERANKCOMPUTER_HPP_ */
