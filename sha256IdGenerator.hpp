#ifndef SRC_SHA256IDGENERATOR_HPP_
#define SRC_SHA256IDGENERATOR_HPP_

#include "immutable/idGenerator.hpp"
#include "immutable/pageId.hpp"

#include <memory>
#include <vector>
//systemowe funkcje
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

class Sha256IdGenerator : public IdGenerator {
public:
    virtual PageId generateId(std::string const& content) const
    {
        std::string input = "echo -n \"" + content + "\" | sha256sum";
        char* cmd = &input[0];
        std::vector<char> buff(64);
        std::string res;

        std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);

        if (!pipe) {
            exit(1);
            //error
        }

        while (fgets(buff.data(), buff.size(), pipe.get()) != nullptr) {
            res += buff.data();
        }

        return PageId(res.substr(0, 64));
    }
};

#endif /* SRC_SHA256IDGENERATOR_HPP_ */
